package com.ek;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;



public class Book {
	private int id;
	
	
	@Size(min=3, max=32,   message="size.book.name")
	private String name;
	
	private String author;
	@Max(value=50000,  message="maxPrice.book.price")
	private Integer price;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	
	

}

package com.ek;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class TestColtroller {
	
	@Autowired
	BookValidator validator2;
	
	@InitBinder
	public void initializeBinder(WebDataBinder binder){
		
		binder.setDisallowedFields("id");
	//	binder.setValidator(validator2);
		
	}
	
	@RequestMapping(value= "/path/{val}")
	public String pathVariable(@PathVariable("val") String val, Model model){
		System.out.println("Inside controller");
		System.out.println("Value of Val: "+val);
		model.addAttribute("testVal",val);
		return "myPage";
	}
	
	@RequestMapping(value="/reqPara")
	public String requestParaMeth(Model model, @RequestParam("para1") String para1, @RequestParam("para2") String para2) {
		System.out.println("Inside RequestPara...");
		model.addAttribute("p1", para1);
		model.addAttribute("p2", para2);
		
		return "paraPage";
		
	}
	
	
	@RequestMapping(value="/addBook1", method= RequestMethod.GET )
	public String addBook(Model model){
		System.out.println("inside get");
		Book bk= new Book();
		System.out.println("id: "+ bk.getId());
		model.addAttribute("attr2", bk);		
		
		return "addBook";
	}
	@RequestMapping(value="/addBook2", method= RequestMethod.POST)
	public String addBook(@ModelAttribute("attr2")@Valid Book bk1,  BindingResult result,Model model){
		
		validator2.validate(bk1, result);
		
		
		String[] suppressedFields= result.getSuppressedFields();
		if(suppressedFields.length >0){
			throw new RuntimeException("Disallowed fields are entered...");
		}
		
		if(result.hasErrors()){
			return "addBook";
		}
		
		System.out.println("inside post");
		String name=bk1.getName();
		model.addAttribute("book", bk1);
		model.addAttribute("name", name);
		return "showBook";
	}

}

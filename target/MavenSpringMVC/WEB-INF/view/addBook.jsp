<%@ page language="java" contentType="text/html; charset=ISO-8859-1"    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Book details</title>
</head>
<body>
<form:form method="POST" modelAttribute="attr2" action="/MavenSpringMVC/addBook2" >
<table>

<tr>
<td>
<spring:message code="addBook.form.name.label" />
</td>
<td>
<form:input path="name"/>
<form:errors path="name" />
</td>
</tr>

<tr>
<td>
<spring:message code="addBook.form.author.label" />
</td>
<td>
<form:input path="author"/>
<form:errors path="author"  />
</td>
</tr>

<tr>
<td>
Price: 
</td>
<td>
<form:input type="text" path="price"  />
<form:errors path="price" />
</td>
</tr>
</table>
<input type="submit">
</form:form>
</body>
</html>